<?php

namespace App\Command;

use App\Service\AsciiService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AsciiCommand extends Command
{
    protected static $defaultName = 'ascii';

    protected function configure()
    {
        $this
            ->setName('ascii')
            ->setDescription('Display word on letter-ascii table')
            ->addArgument('word', InputArgument::REQUIRED, 'Word to transform')
            ->setHelp('This command allows you transform word to ascii-letter table with and argument')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $word = $input->getArgument('word');
        if ($word) {
            $arr_ascii = AsciiService::wordToAscii($word);
            foreach ($arr_ascii as $position) {
                $io->writeln($position["letter"] . ' ' . $position["ascii_number"]);
            }
        }
    }
}
