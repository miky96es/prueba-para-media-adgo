<?php

namespace App\Command;

use App\Service\AsciiService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AsciiDeleteCommand extends Command
{
    protected static $defaultName = 'ascii:delete';

    protected function configure()
    {
        $this
            ->setName('ascii:delete')
            ->setDescription('Delete a word of CSV file by id')
            ->addArgument('id', InputArgument::REQUIRED, 'Id to delete')
            ->setHelp('This command allows you delete info of word in CSV file passing id for argument')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $id = $input->getArgument('id');
        $deleted = AsciiService::deleteById($id);
        if ($deleted) {
            $io->success("Word info with id $id correctly deleted!");
        } else {
            $io->error("Can't delete word info in CSV file with id passed by argument with value $id!");
        }
    }
}
