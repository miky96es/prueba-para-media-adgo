<?php

namespace App\Controller;

use App\Service\AsciiService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AsciiController extends Controller
{

    /**
     * @Route("/ascii/list", name="ascii_list")
     */
    function list() {
        $arr = AsciiService::getCSVasArray(true);
        return $this->render('ascii/list.html.twig', [
            'arr' => $arr,
            'form' => false,
            'saved' => false,
        ]);
    }

    /**
     * @Route("/ascii/manager", name="ascii_manager")
     */
    public function manager(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('word', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Save!'))
            ->getForm();

        $form->handleRequest($request);

        $saved = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            if (AsciiService::saveCSV($data['word'])) {
                $saved = $data['word'];
            }
        }

        $arr = AsciiService::getCSVasArray(true);

        return $this->render('ascii/list.html.twig', [
            'arr' => $arr,
            'form' => $form->createView(),
            'saved' => $saved,
        ]);
    }

    /**
     * @Route("/ascii/{slug}", name="ascii_show")
     */
    public function show($slug)
    {
        $ascii_array = AsciiService::wordToAscii($slug);
        return $this->render('ascii/index.html.twig', [
            'ascii_array' => $ascii_array,
            'word' => $slug,
            'added' => false,
        ]);
    }

    /**
     * @Route("/ascii/remember/{slug}", name="ascii_save")
     */
    public function remember($slug)
    {
        $ascii_array = AsciiService::wordToAscii($slug);
        $added = AsciiService::saveCSV($slug);
        return $this->render('ascii/index.html.twig', [
            'ascii_array' => $ascii_array,
            'word' => $slug,
            'added' => $added,
        ]);
    }
}
