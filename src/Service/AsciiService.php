<?php
namespace App\Service;

use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class created to reuse code in Commands and Controllers and modify CSV file in /public directory
 */
class AsciiService
{
    const FICHERO_ASCIIS = __DIR__ . '\..\..\public\ascii_words.csv';
    /**
     * return an array and in every position have associative array about his
     * corresponcy letter of word passed, with "letter" position and "ascii_number"
     * position which have the correctly letter info.
     * @param string word toke thanks routing /ascii/{slug}
     * @return array return info of word, letter by letter
     */

    public static function wordToAscii($word): array
    {
        $ascii_word = array();
        for ($i = 0; $i < strlen($word); $i++) {
            array_push($ascii_word, array("letter" => $word[$i], "ascii_number" => ord($word[$i])));
        }
        return $ascii_word;
    }

    /**
     * Save array of Ascii
     * @param array word toke thanks routing /ascii/remember/{slug}
     * @return bool
     */
    public static function saveCSV($word): bool
    {
        $response = false;
        $l_number = self::getNewId();
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
        $arr = array($l_number, date('d/m/Y'), $word);
        if (!file_exists(self::FICHERO_ASCIIS)) {
            fopen(self::FICHERO_ASCIIS, "w");
        }
        if (!self::checkWordCSV($word)) {
            if (file_put_contents(self::FICHERO_ASCIIS, implode(',', $arr) . "\n", FILE_APPEND | LOCK_EX)) {
                $response = true;
            }
        }
        return $response;
    }

    /**
     * Check if exists id on file
     * @param int integer to check if exists id in CSV file
     */
    public static function checkIdCSV($id): bool
    {
        $response = false;
        $arr = self::getCSVasArray();
        foreach ($arr as $key => $value) { //overwitting positions of array only with the ids, cleaning array to work easily
            $arr[$key] = $arr[$key][0];
        }
        foreach ($arr as $value) {
            if ($value == $id) {
                return true;
            }
        }
        return $response;
    }

    /**
     * Check if exists word on file
     * @param string string to check if exists word in CSV file
     * @return bool
     */
    public static function checkWordCSV($word): bool
    {
        $response = false;
        $arr = self::getCSVasArray();
        foreach ($arr as $position) {
            if ($position[2] == $word) {
                return true;
            }
        }
        return $response;
    }

    /**
     * Transform the document CSV to array
     * @return array
     */
    public static function getCSVasArray($add = false): array
    {
        $arr = array();
        if (file_exists(self::FICHERO_ASCIIS)) {
            $arr = file(self::FICHERO_ASCIIS);
        }
        foreach ($arr as $key => $value) {
            $arr[$key] = explode(',', $value);
            for ($i = 0; $i < count($arr[$key]); $i++) {
                $arr[$key][2] = trim($arr[$key][2]);
            }
        }
        if ($add) { //on case wich need the sum of ascii codes in the array
            foreach ($arr as $key => $value) {
                array_push($arr[$key], self::sumAsciiword($value[2]));
            }
        }
        return $arr;
    }
    /**
     * Returns the last id and add 1 about CSV  file where data is stored
     * @return int
     */
    public static function getNewId(): int
    {
        $arr = self::getCSVasArray();
        $last = 0;
        foreach ($arr as $key => $value) {
            if ($last < $arr[$key][0]) {
                $last = $arr[$key][0];
            }
        }
        return ++$last;
    }

    /**
     * Returns the sum of ascii codes letter by letter of  word
     * @param string word to sum codes
     * @return int sum of codes
     */
    public static function sumAsciiWord($word): int
    {
        $sum = 0;
        for ($i = 0; $i < strlen($word); $i++) {
            $sum += ord($word[$i]);
        }
        return $sum;
    }

    /**
     * Delete info of word of CSV file by id
     * @param int id to pass
     * @return bool if can delete true else false
     */
    public static function deleteById($id): bool
    {
        $response = false;
        if (self::checkIdCSV($id)) {
            $lines = self::getCSVasArray();
            file_put_contents(self::FICHERO_ASCIIS, '', LOCK_EX);
            for ($i = 0; $i < count($lines); $i++) {
                if ($lines[$i][0] != $id) {
                    file_put_contents(self::FICHERO_ASCIIS, implode(',', $lines[$i]) . "\n", FILE_APPEND | LOCK_EX);
                } else if ($lines[$i][0] == $id) {
                    $response = true;
                }
            }
        }
        return $response;
    }
}
